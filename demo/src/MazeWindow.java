package demo.src;

import javax.swing.*;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 窗口
 */
public class MazeWindow extends JFrame{
    /**
     * 初始化时向不同的键添加信息
     */
    /** 按钮：生成迷宫 **/
    JButton makeMaze=new JButton("生成迷宫");
    /** 按钮：解迷宫 **/
    JButton solveMaze=new JButton("解迷宫");
    /** 提式：迷宫大小 **/
    JLabel mazeSizeLable=new JLabel("迷宫大小");
    /** 修改：迷宫大小 **/
    JTextField mazeSize=new JTextField("100");
    /** 提式：障碍物占比 **/
    JLabel barrierRateLable=new JLabel("障碍率");
    /** 修改：障碍物占比 **/
    JTextField barrierRate=new JTextField("0.6");
    /** 合成的按钮布局 **/
    JPanel buttons=new JPanel();
    JPanel mazePanel=new MazePanel();
    /** 合成的信息窗口布局 **/
    JPanel info=new JPanel();
    /** 图 **/
    char[][] maze;

    /**
     * 初始化布局
     * @param weight 窗口大小
     */
    MazeWindow(int weight){
        //布局控制按钮  生成迷宫、解迷宫
        setSize(weight, weight*6/5);
        buttons.setLayout(new FlowLayout());
        buttons.add(makeMaze);
        buttons.add(solveMaze);

        //布局设置按钮  迷宫大小、修改障碍物占比
        info.setLayout(new FlowLayout());
        info.add(mazeSizeLable);
        info.add(mazeSize);
        info.add(barrierRateLable);
        info.add(barrierRate);
        //合成BorderLayout边界布局
        JPanel jPanel=new JPanel();
        jPanel.add(buttons,BorderLayout.NORTH);
        jPanel.add(info,BorderLayout.SOUTH);
        setLayout(new BorderLayout());

        makeMaze.setPreferredSize(new Dimension(weight*4/25, weight*3/25));
        solveMaze.setPreferredSize(new Dimension(weight*3/25, weight*3/25));
        mazeSize.setPreferredSize(new Dimension(weight*3/25, weight*3/25));
        barrierRate.setPreferredSize(new Dimension(weight*3/25, weight*3/25));

        mazePanel.setPreferredSize(new Dimension(weight, weight));
        makeMaze.setFont(new Font("宋体", Font.PLAIN, 15));
        solveMaze.setFont(new Font("宋体", Font.PLAIN, 15));
        mazeSizeLable.setFont(new Font("宋体", Font.PLAIN, 20));
        mazeSize.setFont(new Font("宋体", Font.PLAIN, 20));
        barrierRateLable.setFont(new Font("宋体", Font.PLAIN, 20));
        barrierRate.setFont(new Font("宋体", Font.PLAIN, 20));
        add(mazePanel, BorderLayout.NORTH);      
        add(jPanel,BorderLayout.SOUTH);

        //设置监听
        makeMaze.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                //监听,读取文本框数据，应用于单例模式下的MazeGenerate对象
                int size=Integer.parseInt(mazeSize.getText());
                double rate=Double.parseDouble(barrierRate.getText());
                maze=MazeGenerate.getMazeGenerate().generate(size, rate, true);
                repaint();
            }
        });
        solveMaze.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                //监听，将maze数据传入A星算法
                AStar.getAStar().search(maze);
                repaint();
            }
        });
    }

    /**
     * 重写的JPanel
     * <p> 对其中paintComponent进行修改
     */
    class MazePanel extends JPanel{
        @Override
        protected void paintComponent(Graphics g) {
            // TODO Auto-generated method stub
            super.paintComponent(g);
            //设置默认的参数
            if(maze==null){
                maze=MazeGenerate.getMazeGenerate().generate(100,0.6,true);
            }
            int weight=getWidth();
            int blockWidth=weight/maze.length;
            //显示信息
            for(int y=0;y<maze.length;y++){
                for(int x=0;x<maze[0].length;x++){
                    if(maze[x][y]=='■'){
                        g.setColor(Color.BLUE);
                        g.fillRect(x*blockWidth, y*blockWidth, blockWidth, blockWidth);
                    }else if(maze[x][y]=='*'){
                        g.setColor(Color.RED);
                        g.fillRect(x*blockWidth, y*blockWidth, blockWidth, blockWidth);
                    }
                }
            }
        }
    }
}
