package demo.src;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.*;

public class App {
    /**
     * 迷宫图形模拟
     * <p> 窗口类生成后监听文本框和按键，由此刷新小程序界面
     * */
    public static void main(String[] args) throws Exception {
        /** 创建窗口类 */
        MazeWindow mazeWindow=new MazeWindow(700);
        mazeWindow.setTitle("迷宫");// 标题
        mazeWindow.setVisible(true);// 将窗口设为可见
        mazeWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// 关闭窗口时退出程序
    }
}
